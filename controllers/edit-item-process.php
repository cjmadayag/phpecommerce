<?php 
	require "connection.php";

function validateForm(){
		$itemName=$_POST["itemName"];
		$itemPrice=$_POST["itemPrice"];
		$itemDescription=$_POST["itemDescription"];
		$itemCategory_id=$_POST["itemCategory"];
		$itemImage = $_FILES["itemImage"];

		$fileTypes = ["jpg","jpeg","png","gif","svg","webp","bitmap","tiff","tif"];

		$imageExt = strtolower(pathinfo($itemImage["name"], PATHINFO_EXTENSION));

		$errors = 0;
		if(!isset($itemName) || $itemName==""){
			$errors++;
		}
		if(!isset($itemPrice) || $itemPrice<0){
			$errors++;
		}
		if(!isset($itemDescription) || $itemDescription==""){
			$errors++;
		}
		if(!isset($itemCategory_id) || $itemCategory_id==""){
			$errors++;
		}
		if($_FILES["itemImage"]["size"]>0 && !in_array($imageExt,$fileTypes)){
			$errors++;
		}
		if($errors>0){
			return false;
		}
		else{
			return true;
		}
	}

	if (validateForm()) {
		$itemId = $_POST["itemId"];
		$itemName=$_POST["itemName"];
		$itemPrice=$_POST["itemPrice"];
		$itemDescription=$_POST["itemDescription"];
		$itemCategory_id=$_POST["itemCategory"];

		$itemImage = "";

		$image_query = "SELECT * from items where id=$itemId";

		$image = mysqli_fetch_assoc(mysqli_query($conn,$image_query));

		if($_FILES["itemImage"]["name"]==""){
			$itemImage = $image["imgPath"];
		}
		else{
			$destination = "../assets/images/";
			$fileName = $_FILES["itemImage"]["name"];
			$itemImage = $destination . $fileName;
			move_uploaded_file($_FILES["itemImage"]["tmp_name"], $itemImage);
		}

		$update_query = "UPDATE items set name='$itemName',price=$itemPrice,description='$itemDescription',imgPath='$itemImage',category_id=$itemCategory_id where id=$itemId";
		$update = mysqli_query($conn,$update_query);

		header("location: ../views/catalog.php");
	}
	else{
		header("location:".$_SERVER["HTTP_REFERER"]);
	}
?>