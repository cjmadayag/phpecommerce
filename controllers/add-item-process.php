<?php 
	require "connection.php";

	$itemName=$_POST["itemName"];
	$itemPrice=$_POST["itemPrice"];
	$itemDescription=$_POST["itemDescription"];
	$itemCategory_id=$_POST["itemCategory"];
	$itemImage = $_FILES["itemImage"];

	$fileTypes = ["jpg","jpeg","png","gif","svg","webp","bitmap","tiff","tif"];

	$imageExt = strtolower(pathinfo($itemImage["name"], PATHINFO_EXTENSION));

	function validateForm(){
		$itemName=$_POST["itemName"];
		$itemPrice=$_POST["itemPrice"];
		$itemDescription=$_POST["itemDescription"];
		$itemCatergory_id=$_POST["itemCategory"];
		$itemImage = $_FILES["itemImage"];

		$fileTypes = ["jpg","jpeg","png","gif","svg","webp","bitmap","tiff","tif"];

		$imageExt = strtolower(pathinfo($itemImage["name"], PATHINFO_EXTENSION));

		$errors = 0;
		if(!isset($itemName) || $itemName==""){
			$errors++;
		}
		if(!isset($itemPrice) || $itemPrice<0){
			$errors++;
		}
		if(!isset($itemDescription) || $itemDescription==""){
			$errors++;
		}
		if(!isset($itemCatergory_id) || $itemCatergory_id==""){
			$errors++;
		}
		if(!isset($itemImage) || $itemImage == ""){
			$errors++;
		}
		if(!in_array($imageExt,$fileTypes)){
			$errors++;
		}
		if($errors>0){
			return false;
		}
		else{
			return true;
		}
	}

	if(validateForm()){
		$destination = "../assets/images/";
		$imgFileName = $itemImage["name"];
		$imgPath = $destination.$imgFileName;

		move_uploaded_file($itemImage["tmp_name"],$imgPath);

		$addItem_query = "insert into items(name,price,description,imgPath,category_id) values ('$itemName','$itemPrice','$itemDescription','$imgPath','$itemCategory_id')";

		$new_item=mysqli_query($conn,$addItem_query);

		// var_dump($new_item);

		header("location: ../views/catalog.php");
	}
	else{
		header("location:" . $_SERVER["HTTP_REFERER"]);
	}
?>

