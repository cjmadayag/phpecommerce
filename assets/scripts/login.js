let loginUserBtn = document.getElementById("loginUser")

loginUserBtn.addEventListener("click",()=>{
	let email = document.getElementById("email").value;
	let password = document.getElementById("password").value;

	let data = new FormData;
	data.append("email",email);
	data.append("password",password);

	fetch("../controllers/login-process.php",{
		method: "post",
		body:data
	}).then(res=>res.text()).then(res=>{
		if(res=="login_failed"){
			document.getElementById("email").nextElementSibling.textContent = "Please provide valid credentials :/"
		}else{
			window.location.replace("catalog.php")
		}
	})
})