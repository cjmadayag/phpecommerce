let	spanQs = document.querySelectorAll(".spanQ");

console.log(spanQs);

spanQs.forEach(indiv_spanQ=>{
	indiv_spanQ.parentElement.addEventListener("click", ()=>{
		indiv_spanQ.nextElementSibling.classList.remove("d-none");
		indiv_spanQ.classList.add("d-none");
	})

	let quantity;

	indiv_spanQ.nextElementSibling.lastElementChild.addEventListener("keypress", e=>{
		quantity = indiv_spanQ.nextElementSibling.lastElementChild.value
		if(e.keyCode == 13 && quantity <= 0){
			e.preventDefault();
			alert("Invalid Quantity");
			indiv_spanQ.classList.remove("d-none");
			indiv_spanQ.nextElementSibling.classList.add("d-none");
			indiv_spanQ.nextElementSibling.lastElementChild.value = indiv_spanQ.textContent;
		}
	})

	indiv_spanQ.nextElementSibling.lastElementChild.addEventListener("blur", ()=>{
		quantity = indiv_spanQ.nextElementSibling.lastElementChild.value
		if(quantity<=0){
			alert("Invalid Quantity");
			indiv_spanQ.classList.remove("d-none"); 
			indiv_spanQ.nextElementSibling.classList.add("d-none");
			
		}
		else{
			indiv_spanQ.nextElementSibling.submit();
		}

		// let id = indiv_spanQ.nextElementSibling.getAttribute("data-id");
		// let quantity = indiv_spanQ.nextElementSibling.value;
		// let price = parseInt(indiv_spanQ.parentElement.previousElementSibling.textContent);

		// let data = new FormData;
		// data.append("itemId",id);
		// data.append("itemQty",quantity);
		// data.append("fromCartPage","fromCartPage");

		// fetch("../../controllers/add-to-cart-process.php",{
		// 	method: "POST",
		// 	body: data
		// }).then(res=> res.text()).then(res=>{
		// 	indiv_spanQ.classList.remove("d-none");
		// 	indiv_spanQ.nextElementSibling.classList.add("d-none");
		// 	indiv_spanQ.textContent = quantity;
		// 	indiv_spanQ.parentElement.nextElementSibling.textContent = price*quantity;
		// 	document.getElementById("cartCount").textContent = res;
		// })
	})

})