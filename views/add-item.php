<?php 
	require "../partials/template.php";

	function getTitle(){
		echo "Add Item Form";
	}

	function getBodyContents(){
	?>
	<h1 class="text-center py-5">ADD ITEM FORM</h1>

	<div class="container">
		<div class="col-lg-6 offset-lg-3">
			<form method="POST" enctype="multipart/form-data" action="../controllers/add-item-process.php">
				<div class="form-group">
					<label>Item Name</label>
					<input type="text" name="itemName" class="form-control">
				</div>
				<div class="form-group">
					<label>Price</label>
					<input type="number" name="itemPrice" class="form-control">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea class="form-control" name="itemDescription"></textarea>
				</div>
				<div class="form-group">
					<label>Image</label>
					<input type="file" name="itemImage" class="form-control">
				</div>
				<div class="form-group">
					<label>Category</label>
					<select name="itemCategory" class="form-control">
					<?php 
						require "../controllers/connection.php";
						$categories_query= "select * from categories";
						$categories = mysqli_query($conn, $categories_query);

						foreach($categories as $category){
						?>	
							<option value="<?= $category["id"]; ?>"><?= $category["name"]; ?></option>

						<?php
						}
					?>
					</select>
				</div>
				<div class="text-center">
					<button type="submit" class="btn btn-info">Add Item</button>
				</div>
			</form>
		</div>
	</div>
	<?php
	}
?>