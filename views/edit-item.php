<?php 
	require "../partials/template.php";

	function getTitle(){
		echo "Edit Item Form";
	}

	function getBodyContents(){
		require "../controllers/connection.php";

		$id = $_GET["id"];
		$item_query = "select * from items where id = $id";
		$item = mysqli_fetch_assoc(mysqli_query($conn,$item_query));

	?>
	<h1 class="text-center py-5">EDIT ITEM FORM</h1>

	<div class="container">
		<div class="col-lg-6 offset-lg-3">
			<form method="POST" enctype="multipart/form-data" action="../controllers/edit-item-process.php">
				<div class="form-group">
					<label>Item Name</label>
					<input
						type="text"
						name="itemName"
						class="form-control"
						value="<?= $item["name"]?>">
				</div>
				<div class="form-group">
					<label>Price</label>
					<input
						type="number"
						name="itemPrice"
						class="form-control"
						value="<?= $item["price"]?>">
				</div>
				<div class="form-group">
					<label>Description</label>
					<textarea
						class="form-control"
						name="itemDescription"><?= $item["description"]?>
					</textarea>
				</div>
				<div class="form-group">
					<label>Image</label>
					<input
						type="file"
						name="itemImage"
						class="form-control">
				</div>
				<div class="form-group">
					<label>Category</label>
					<select name="itemCategory" class="form-control">
					<?php 
						require "../controllers/connection.php";
						$categories_query= "select * from categories";
						$categories = mysqli_query($conn, $categories_query);

						foreach($categories as $category){
						?>	
							<option value="<?= $category["id"]; ?>"
								<?php 
									echo $category["id"]==$item["category_id"] ? "selected" : "";?>
									><?= $category["name"]; ?></option>
						<?php
						}
					?>
					</select>
				</div>
				<input type="hidden" name="itemId" value="<?= $id ?>">
				<div class="text-center">
					<button type="submit" class="btn btn-info">Edit Item</button>
				</div>
			</form>
		</div>
	</div>
	<?php
	}
?>