<?php 
	require "../partials/template.php";

	function getTitle(){
		echo "Cart";
	}

	function getBodyContents(){
		require "../controllers/connection.php";
	?>
		<h1 class="text-center py-5">CART PAGE</h1>
		<div class="col-lg-10 offset-lg-1">
			<table class="table table-striped table-bodered">
				<thead>
					<tr class="text-center">
						<th>Item</th>
						<th>Price</th>
						<th>Quantity</th>
						<th>Subtotal</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<?php 
						$total = 0;
						if(isset($_SESSION["itemQty"])){
							// var_dump($_SESSION["itemQty"]);
							foreach ($_SESSION["itemQty"] as $itemId => $itemQty) {
								$item_query = "select * from items where id = $itemId";
								$item = mysqli_fetch_assoc(mysqli_query($conn,$item_query));
								$subtotal = $item["price"] * $itemQty;
								$total +=$subtotal;
							?>
								<tr class="text-center">
									<td><?= $item["name"]; ?></td>
									<td><?= $item["price"]; ?></td>
									<td>
										<span class="spanQ"><?= $itemQty; ?></span>
										<form action="../controllers/add-to-cart-process.php" method="post" class="d-none">
											<input type="hidden" name="itemId" value="<?= $itemId ?>">
											<input type="hidden" name="fromCartPage" value="fromCartPage">
											<input type="number" class="form-control" name="itemQty" value="<?= $itemQty; ?>" data-id="<?= $itemId; ?>">
										</form>
									</td>
									<td><?= number_format($subtotal,2); ?></td>
									<td><a href="../controllers/remove-item-from-cart-process.php?id=<?= $itemId ?>" class="btn btn-danger">Delete</a></td>
								</tr>
							<?php
							}
						}
					?>
				</tbody>
				<tfoot>
					<tr class="text-center">
						<td></td>
						<td></td>
						<td class="font-weight-bold">Total</td>
						<td class="font-weight-bold" id="totalPayment"><?= number_format($total,2) ?></td>
						<td><a href="../controllers/empty-cart-process.php" class="btn btn-danger">Empty Cart</a></td>
					</tr>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td><div id="paypal-button-container"></div></td>
						<td></td>
					</tr>
				</tfoot>
			</table>
		</div>
		<script type="text/javascript" src="../assets/scripts/update-cart.js"></script>
		 <script
    src="https://www.paypal.com/sdk/js?client-id=AftD5XJgCzi5VrP3mya_SGBsYi4PqUhxtkzLckhXzEuTQnuKgqb5sjb4vtveAWipPxO1w1S6t0rbWmNT"> // Required. Replace SB_CLIENT_ID with your sandbox client ID.
  </script>
  <script>
  	let totalPayment=document.getElementById("totalPayment").textContent.split(",").join("");
  	console.log(totalPayment);
    paypal.Buttons({
    createOrder: function(data, actions) {
      // This function sets up the details of the transaction, including the amount and line item details.
      return actions.order.create({
        purchase_units: [{
          amount: {
            value: totalPayment
          }
        }]
      });
    },
  onApprove: function(data, actions) {
      // This function captures the funds from the transaction.
      return actions.order.capture().then(function(details) {
        // This function shows a transaction success message to your buyer.
        let data = new FormData;
        data.append("totalPayment",totalPayment);
        data.append("fromPaypal","fromPaypal");

        fetch("../controllers/checkout-process.php",{
        	method: "post",
        	body: data
        }).then(res=>res.text()).then(res=>{
        	console.log(res)
        	alert('Transaction completed by ' + details.payer.name.given_name)
        })

      });
    }
  }).render('#paypal-button-container');
    // This function displays Smart Payment Buttons on your web page.
  </script>
	<?php
	}
?>