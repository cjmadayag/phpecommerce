<?php 
	require '../partials/template.php';

	function getTitle(){
		echo "Register";
	}

	function getBodyContents(){
	?>
		<h1 class="text-center py-5">Register</h1>
		<div class="col-lg-6 offset-lg-3">
			<form action="" method="POST">
				<div class="form-group">
					<label for="firstName">First Name</label>
					<input type="text" name="firstName" id="firstName" class="form-control">
					<span class="validation"></span>
				</div>
				<div class="form-group">
					<label for="lastName">Last Name</label>
					<input type="text" name="lastName" id="lastName" class="form-control">
					<span class="validation"></span>
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" name="email" id="email" class="form-control">
					<span class="validation"></span>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" class="form-control">
					<span class="validation"></span>
				</div>
				<div class="form-group">
					<label for="confirmPassword">Confirm Password</label>
					<input type="password" name="confirmPassword" id="confirmPassword" class="form-control">
					<span class="validation"></span>
				</div>
				<button type="button" class="btn btn-info" id="registerUser">Register</button>
				<p>Already Registered? <a href="login.php">Login</a></p>
			</form>
		</div>
		<script type="text/javascript" src="../assets/scripts/register.js"></script>


	<?php
	}
?>