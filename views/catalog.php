<?php 
	require "../partials/template.php";
	function getTitle(){
		echo "Catalog";
	}

	function getBodyContents(){
	?>
		<h1 class="text-center py-5">Bar Menu</h1>

		<!-- Item List -->
		<div class="row">
			<?php 
				require "../controllers/connection.php";
				$items_query = "Select * from items";
				$items = mysqli_query($conn,$items_query);

				foreach ($items as $item) {
				?>
					<div class="col-lg-4 py-2">
						<div class="card">
							<img class="card-img-top" height="200px" src="<?php echo $item["imgPath"] ;?>"></img>
							<div class="card-body">
								<h4 class="card-body text-center">
									<?= $item["name"]; ?>
								</h4>
								<p class="card-text">Price: Php <?= $item["price"]; ?></p>
								<p class="card-text">Description: <?= $item["description"]; ?></p>
								<p class="card-text">Category: 
								<?php
									$categoryId = $item["category_id"];

									$category_query = "select * from categories where id=$categoryId";
									$category = mysqli_fetch_assoc(mysqli_query($conn,$category_query));	
									echo $category["name"];
								?>
								</p>
							</div>
							<div class="card-footer">
								<a class="btn btn-danger" href="../controllers/delete-item-process.php?id=<?= $item["id"] ?>">Delete Item</a>
								<a class="btn btn-success" href="edit-item.php?id=<?= $item["id"] ?>">Edit Item</a>
							</div>
							<div class="card-footer">
								<!-- <form method="POST" action="../controllers/add-to-cart-process.php"> -->
									<!-- <input type="hidden" name="itemId" value="$item["id"]"> -->
									<input type="number" name="itemQty" class="form-control" value="1">
									<button type="button" class="btn btn-primary addToCart" data-id="<?= $item["id"];?>">Add to Cart</button>
								<!-- </form> -->
							</div>
						</div>
					</div>
				<?php
				}
			?>
		</div>
		<script type="text/javascript" src="../assets/scripts/add-to-cart.js"></script>
	<?php
	}

?>