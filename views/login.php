<?php 
	require '../partials/template.php';

	function getTitle(){
		echo "Login";
	}

	function getBodyContents(){
	?>
		<h1 class="text-center py-5">Login</h1>
		<div class="col-lg-6 offset-lg-3">
			<form action="" method="POST">
				<div class="form-group">
					<label for="email">Email</label>
					<input type="email" name="email" id="email" class="form-control">
					<span class="validation"></span>
				</div>
				<div class="form-group">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" class="form-control">
					<span class="validation"></span>
				</div>
				<button type="button" class="btn btn-info" id="loginUser">Login</button>
				<p>Not yet Registered? <a href="login.php">Register</a></p>
			</form>
		</div>
		<script type="text/javascript" src="../assets/scripts/login.js"></script>
	<?php
	}
?>